int i;
int sign;
int value;
float result;
int datapin = 15;
int clockpin = 14;  
unsigned long tempmicros;
bool bitval[24];

 
void setup() {
    Serial.begin(9600);
    pinMode(clockpin, INPUT);
    pinMode(datapin, INPUT);
}

void loop () {
    while (digitalRead(clockpin) == LOW) {} //wait for the end of the HIGH pulse
    tempmicros = micros();
    while (digitalRead(clockpin) == HIGH) {} // wait until we go high again (skips first bit)
    if ((micros()-tempmicros)>500) {
        decode(); //decode the bit sequence
    }
}

void decode() {
  sign=1;
  value=0;

  for (i=0; i<23; i++) {
    while (digitalRead(clockpin) == LOW) { } //wait until clock returns to HIGH- the first bit is not needed
    while (digitalRead(clockpin) == HIGH) {} //wait until clock returns to LOW
    if (digitalRead(datapin) == HIGH) { // in original example an NPN transistor was used which flipped the output loic (low = 1, high = 0). If using a proper logic level shifter this would be reveresed 
        // So if we are reading a bit
        if (i<20) { // And if we are within the first 20 bits of data
            bitval[i] = 1;
            value|= 1<<i; // Shift the bit to its correct order of magnitude (see bit shifting), and add it (using the or operator) with value (which is acting like a sum)
        }
        if (i==20) { // The 21st bit is the sign bit, which if 1 means we are reading a negative value
            sign=-1;
        }
     }
   else {
      bitval[i] = 0;
   }
  }
  result=(value*sign)/100.00;    
  Serial.println(result,2); //print result with 2 decimals
  /*
  for (i=0; i<23; i++) {
      Serial.print(bitval[i]);
  }
  Serial.println("");
  */
  //delay(1000);
}
