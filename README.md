# Serial Caliper

Many of the ubiquitous $20 calipers found on amazon and ebay share a relatively accessible serial port and protocol tucked behind the battery compartment. This means that with a level shifter (the calipers run a single coin cell resulting in 1.6V logic level) it's possible to get 10um linear measurement resolution to a microcontroller without having to resort to more expensive linear encoders.

<img src="./images/caliper_port.png" width=600 />

The caveats are that these calipers are only running at only about 2sps, so not useful for real time loop closing, and the protocol is peripheral-less and must be bit-banged which eats up MCU resources.

Here's a [good reference](https://sites.google.com/site/marthalprojects/home/arduino/arduino-reads-digital-caliper) for getting initial results. I made a few changes in code including printing binary packets for debugging, and flipping all of the reads (we are using a level shifter not a transistor). See below for a simple breadboarded circuit to get up and running.

<img src="./images/calipers_breadboard.png" width=600 />

Raw scope readings (reading a 0.01mm) can be seen below, transmitted over seven 4-bit chunks. The readings are fixed point with the two LSB in decimal being devoted to 100 and 10 um significant figures. The 21st bit is devoted to measurement sign, and one of the remaining bits is used for unit conversion, while some are unused.

<img src="./images/caliper_scope.png" width=600 />
